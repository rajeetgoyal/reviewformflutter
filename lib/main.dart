import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'models/review_data/review_data_model.dart';

void main() => runApp(MyApp());

enum TextFormFieldName { DISPLAY_NAME, REVIEW_TITLE, REVIEW }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Lumina Flutter Add Review';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: ReviewForm(),
      ),
    );
  }
}

// Create a Form widget.
class ReviewForm extends StatefulWidget {
  @override
  ReviewFormState createState() {
    return ReviewFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class ReviewFormState extends State<ReviewForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<ReviewFormState>.
  final _formKey = GlobalKey<FormState>();
  String _formType = "Add";
  String _displayName;
  String _reviewTitle;
  double _rating = 0.0;
  bool _ratingErrorVisibility = false;
  String _review;
  AnimationController _animationController;

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(10.0, 15.0, 15.0, 0.0),
              child: IconButton(
                icon: Icon(
                  Icons.close,
                  size: 30.0,
                ),
                onPressed: () {
                  SystemNavigator.pop();
                },
              )),
          Container(
              margin: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 20.0),
              child: Text(
                '$_formType' + ' a review',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.blue,
                    fontSize: 30.0),
              )),
          textLabelWidget('Display Name'),
          textInputWidget(1, 50, TextFormFieldName.DISPLAY_NAME),
          textLabelWidget('Review Title'),
          textInputWidget(1, 100, TextFormFieldName.REVIEW_TITLE),
          Container(
            margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
            child: RatingBar.builder(
              minRating: 1,
              direction: Axis.horizontal,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (double rating) {
                _rating = rating;
              },
            ),
          ),
          Visibility(
            visible: _ratingErrorVisibility,
            child: Container(
              margin: EdgeInsets.fromLTRB(30.0, 5.0, 20.0, 10.0),
              child: Text(
                'Please provide a rating',
                style: TextStyle(color: Colors.red[700], fontSize: 12),
              ),
            ),
          ),
          textLabelWidget('Review'),
          textInputWidget(5, 1000, TextFormFieldName.REVIEW),
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(20.0),
                  child: ElevatedButton(
                    onPressed: () {
                      // Taking the focus away from input fields.
                      FocusScope.of(context).requestFocus(new FocusNode());

                      validateRatingInput();

                      // Validate returns true if the form is valid, or false
                      // otherwise.
                      if (_formKey.currentState.validate() &&
                          _ratingErrorVisibility == false) {
                        // If the form is valid, display a Snackbar.
                        var reviewData = ReviewData(
                            displayName: _displayName,
                            reviewTitle: _review,
                            rating: _rating,
                            review: _review);
                        String json = reviewData.toJson().toString();
                        Scaffold.of(context)
                            .showSnackBar(SnackBar(content: Text(json)));
                      }
                    },
                    child: Text('Save'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  // Returns the layout widget for label of form fields.
  Widget textLabelWidget(String labelTitle) {
    return Container(
      child: Text(
        labelTitle,
        style: TextStyle(color: Colors.black),
      ),
      margin: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 0.0),
    );
  }

  // Returns the layout widget for text input fields.
  Widget textInputWidget(
      int maxLines, int maxLength, TextFormFieldName textFormFieldName) {
    return Container(
      margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 15.0),
      child: TextFormField(
        maxLines: maxLines,
        maxLength: maxLength,
        decoration: InputDecoration(
          counter: Container(),
          border: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.blue,
          )),
        ),
        validator: (value) {
          return validateTextFieldInput(value, textFormFieldName);
        },
      ),
    );
  }

  // Validates the rating provided by the user
  void validateRatingInput() {
    if (_rating == 0.0 && _ratingErrorVisibility == false) {
      setState(() {
        _ratingErrorVisibility = true;
      });
    }
    if (_rating != 0.0 && _ratingErrorVisibility == true) {
      setState(() {
        _ratingErrorVisibility = false;
      });
    }
  }

  // Validates the input of text form fields
  // If validation fails, return string error message.
  // If validation succeeds, return null.
  String validateTextFieldInput(
      var value, TextFormFieldName textFormFieldName) {
    // Validation failed
    if (value.trim().isEmpty) {
      return 'Please enter some text';
    }
    // Validation success
    else {
      switch (textFormFieldName) {
        case TextFormFieldName.DISPLAY_NAME:
          _displayName = value;
          break;
        case TextFormFieldName.REVIEW_TITLE:
          _reviewTitle = value;
          break;
        case TextFormFieldName.REVIEW:
          _review = value;
          break;
      }
      return null;
    }
  }
}
