
import 'package:json_annotation/json_annotation.dart';

part 'review_data_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ReviewData {
  final String displayName;
  final String reviewTitle;
  final double rating;
  final String review;
  ReviewData({this.displayName, this.reviewTitle, this.rating, this.review});
  factory ReviewData.fromJson(Map<String, dynamic> json) => _$ReviewDataFromJson(json);
  Map<String, dynamic> toJson() => _$ReviewDataToJson(this);
}
