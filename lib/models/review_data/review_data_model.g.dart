// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReviewData _$ReviewDataFromJson(Map<String, dynamic> json) {
  return ReviewData(
    displayName: json['displayName'] as String,
    reviewTitle: json['reviewTitle'] as String,
    rating: (json['rating'] as num)?.toDouble(),
    review: json['review'] as String,
  );
}

Map<String, dynamic> _$ReviewDataToJson(ReviewData instance) =>
    <String, dynamic>{
      'displayName': instance.displayName,
      'reviewTitle': instance.reviewTitle,
      'rating': instance.rating,
      'review': instance.review,
    };
